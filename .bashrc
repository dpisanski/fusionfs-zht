# Sample .bashrc for SuSE Linux
# Copyright (c) SuSE GmbH Nuernberg

# There are 3 different types of shells in bash: the login shell, normal shell
# and interactive shell. Login shells read ~/.profile and interactive shells
# read ~/.bashrc; in our setup, /etc/profile sources ~/.bashrc - thus all
# settings made here will also take effect in a login shell.
#
# NOTE: It is recommended to make language settings in ~/.profile rather than
# here, since multilingual X sessions would not work properly if LANG is over-
# ridden in every subshell.

# Some applications read the EDITOR variable to determine your favourite text
# editor. So uncomment the line below and enter the editor of your choice :-)
#export EDITOR=/usr/bin/vim
#export EDITOR=/usr/bin/mcedit

# For some news readers it makes sense to specify the NEWSSERVER variable here
#export NEWSSERVER=your.news.server

# If you want to use a Palm device with Linux, uncomment the two lines below.
# For some (older) Palm Pilots, you might need to set a lower baud rate
# e.g. 57600 or 38400; lowest is 9600 (very slow!)
#
#export PILOTPORT=/dev/pilot
#export PILOTRATE=115200

test -s ~/.alias && . ~/.alias || true
export PS1='[\W@\H]$' 
#alias ls="ls -lht"
#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/dongfang/fusionFS/src/udt/src:/home/dongfang/fusionFS/src/ffsnet:/usr/local/lib:/home/dongfang/gbuf/lib

#export PATH=/home/dongfang/fusionsim/install/darshan/bin:/home/dongfang/software/bin:/home/dongfang/bin/bin:/home/dongfang/download/jdk1.7.0/bin:$PATH
export JAVA_HOME=/home/dongfang/download/jdk1.7.0/bin/java

#this is for ROSS
#export ARCH=x86_64
#export CC=mpicc

#for Swift
export PATH=/home/dongfang/download/swift-0.95-RC6/bin:$PATH

#for Perl 520
export PATH=$HOME/software/perl/bin:$PATH

#for zht_xiaobing
export PATH=$HOME/software/bin:$PATH
export USER_LIB=/home/dongfang/software/lib/
export USER_INCLUDE=/home/dongfang/software/include/
export LD_LIBRARY_PATH=/home/dongfang/software/lib/:$LD_LIBRARY_PATH
